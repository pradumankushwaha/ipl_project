//1. Number of matches played per year of all the years in IPL.
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class MatchesPlayedPerYear {
    public static void main(String[] args) {
        Map<String, Integer> matchPerYear = new TreeMap<>();
        String path = "src/archive/matches.csv";
        BufferedReader br = null;
        String line = "";
        String splitBy = ",";
        int count = 0;
        try {
            br = new BufferedReader(new FileReader(path));
            while ((line = br.readLine()) != null) {
                if (count == 0) {
                    ++count;
                } else {
                    String[] match = line.split(splitBy);
                    if (matchPerYear.containsKey(match[1])) {
                        matchPerYear.put(match[1], matchPerYear.get(match[1]) + 1);
                    } else {
                        matchPerYear.put(match[1], 1);
                    }
                }
            }
            System.out.println("Number of matches played per year in IPL : " +matchPerYear);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    }
