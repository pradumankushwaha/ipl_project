//3. For the year 2016 get the extra runs conceded per team.
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
public class ExtraRunPerTeam {
    public static void main(String[] args) {
        String matchPath = "src/archive/matches.csv";
        String deliveryPath = "src/archive/deliveries.csv";
        String line = "";
        ArrayList matchId =new ArrayList();
        Map<String,Integer> extraRunCount = new TreeMap<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(matchPath));
            br.readLine();
            while((line = br.readLine()) != null){
                String[] values = line.split(",");
                if(values[1].equals("2016")){
                    matchId.add(values[0]);
                }
            }

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }catch (IOException e) {
            throw new RuntimeException(e);
        }
        try{
            BufferedReader br =new BufferedReader(new FileReader(deliveryPath));
            br.readLine();
            while((line = br.readLine()) != null) {
                String[] values = line.split(",");
                String deliveryId = values[0];
                String bowlingTeam = values[3];
                int exraRuns = Integer.parseInt(values[16]);
                if (matchId.contains(deliveryId)) {
                    if (!(extraRunCount.containsKey(bowlingTeam))) {
                        extraRunCount.put(bowlingTeam, 0);
                    }

                    if (extraRunCount.containsKey(bowlingTeam)) {
                        int count = extraRunCount.get(bowlingTeam) + exraRuns;
                        extraRunCount.put(bowlingTeam, count);
                    }
                }
            }
                System.out.println(" For the year 2016 get the extra runs conceded per team : "+extraRunCount);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }catch (IOException e) {
            throw new RuntimeException(e);
        }
        }
    }