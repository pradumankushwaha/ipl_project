// 2. Number of matches won of all teams over all the years of IPL.
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class MatchWonAllYear {
    public static void main(String[] args) {
        Map<String, Integer> matchPerYear = new TreeMap<>();
        String path = "src/archive/matches.csv";
        BufferedReader br = null;
        String line = "";
        String splitBy = ",";
        int count = 0;
        try {
            br = new BufferedReader(new FileReader(path));
            while ((line = br.readLine()) != null) {
                if (count == 0) {
                    ++count;
                } else {
                    String[] match = line.split(splitBy);
                    if (matchPerYear.containsKey(match[10])) {
                        matchPerYear.put(match[10], matchPerYear.get(match[10]) + 1);
                    } else {
                        matchPerYear.put(match[10], 1);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        matchPerYear.remove("");
        System.out.println("Number of matches won of all teams all the years in IPL : " + matchPerYear);
    }
}


